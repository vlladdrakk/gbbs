use std::fs;
use handlebars::{Handlebars, no_escape};
use std::collections::HashMap;
use log::*;
use crate::Request;

pub enum RenderError {
  ViewNotFound
}

pub fn get_view_contents(name: &str) -> std::io::Result<String> {
  let base_dir = "./views";

  return fs::read_to_string(format!("{}/{}.gmi", base_dir, name));
}

pub fn render_view(request: &Request) -> String {
  let data = insert_request_data(request);
  let mut rendered_content = render_to_string(&request.view, &data);
  // Add navigation
  let navigation = render_to_string("components/navigation", &data);
  rendered_content.push_str(&navigation);

  // Add header
  let header = render_to_string("components/header", &data);
  rendered_content.insert_str(0, &header);

  return rendered_content;
}

pub fn render_to_string(view: &str, _data: &HashMap<String, String>) -> String {
  let data = insert_defaults(_data);
  let mut handlebars = Handlebars::new();
  handlebars.register_escape_fn(no_escape);
  let source = get_view_contents(view)
    .expect("Failed to read view file");

  handlebars
    .register_template_string(view, source)
    .unwrap();


  return handlebars.render(view, &data).unwrap();
}

fn insert_request_data(request: &Request) -> HashMap<String, String> {
  let mut tmp_hash = insert_defaults(&request.render_data);
  tmp_hash.insert(String::from("host"), String::from("localhost"));
  tmp_hash.insert(String::from("logged_in"), match request.logged_in {
    true => String::from("true"),
    false => String::from("")
  });
  tmp_hash.insert(String::from("has_cert"), match request.cert {
    Some(_) => String::from("true"),
    None => String::from("false")
  });

  match &request.user {
    Some(user) => {
      let username = user.username.as_ref().unwrap_or(&String::new()).clone();
      tmp_hash.insert(String::from("username"), username);
      tmp_hash.insert(String::from("not_valid_after"), user.not_valid_after.to_string());
      tmp_hash.insert(String::from("not_valid_before"), user.not_valid_before.to_string());
      tmp_hash.insert(String::from("common_name"), user.common_name.clone());
    },
    None => {}
  }

  return tmp_hash;
}

fn insert_defaults(data: &HashMap<String, String>) -> HashMap<String, String> {
  let mut tmp_hash = data.clone();
  tmp_hash.insert(String::from("host"), String::from("localhost"));

  return tmp_hash;
}

pub fn render(request: &Request) -> Result<String, RenderError> {
  info!("Finding render function for view: {}", request.view);

  if request.view == "" {
    Err(RenderError::ViewNotFound)
  } else {
    Ok(render_view(request))
  }
}
