# Account creation complete!

Thanks for creating your account, you can now post and reply :)
{{ #if logged_in }}

New username: {{ username }}

{{ /if }}