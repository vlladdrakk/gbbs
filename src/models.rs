use crate::schema::{posts, users};
use chrono::NaiveDateTime;

#[derive(Queryable, Clone)]
pub struct Post {
    pub id: i32,
    pub title: String,
    pub body: String,
    pub created_at: NaiveDateTime,
    pub reply_to_id: Option<i32>,
    pub user_id: i32
}

#[derive(Insertable)]
#[table_name = "posts"]
pub struct PostForm<'a> {
  pub title: &'a str,
  pub body: &'a str,
  pub user_id: &'a i32
}

#[derive(Queryable, Clone)]
pub struct User {
  pub id: i32,
  pub username: Option<String>,
  pub cert_fingerprint: String,
  pub not_valid_before: NaiveDateTime,
  pub not_valid_after: NaiveDateTime,
  pub common_name: String,
  pub created_at: NaiveDateTime
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct UserForm<'a> {
  pub cert_fingerprint: &'a String,
  pub not_valid_before: &'a NaiveDateTime,
  pub not_valid_after: &'a NaiveDateTime,
  pub common_name: &'a String
}
