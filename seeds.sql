INSERT INTO users (id, username, cert_fingerprint, not_valid_before, not_valid_after, common_name, created_at) VALUES
(1, 'user1', 'mn9TtxL8Ngls7VOjlXUEzPI1wdojDBSGqskPUARQ+m0=', '2022-03-15 19:30:33', '2100-01-01 04:00:00', 'BBS', '2022-03-01 16:27:57'),
(2, 'user2', 'mn1TtxL8Ngls7VOjlXUEzPI1wdojDBSGqskPUARQ+m0=', '2022-03-15 19:30:33', '2100-01-01 04:00:00', 'BBS', '2022-03-02 16:27:57'),
(3, 'user3', 'mn2TtxL8Ngls7VOjlXUEzPI1wdojDBSGqskPUARQ+m0=', '2022-03-15 19:30:33', '2100-01-01 04:00:00', 'BBS', '2022-03-03 16:27:57'),
(4, 'user4', 'mn3TtxL8Ngls7VOjlXUEzPI1wdojDBSGqskPUARQ+m0=', '2022-03-15 19:30:33', '2100-01-01 04:00:00', 'BBS', '2022-03-04 16:27:57'),
(5, 'user5', 'mn4TtxL8Ngls7VOjlXUEzPI1wdojDBSGqskPUARQ+m0=', '2022-03-15 19:30:33', '2100-01-01 04:00:00', 'BBS', '2022-03-05 16:27:57');

INSERT INTO posts (id, title, body, created_at, reply_to_id, user_id) VALUES
(1, 'Post 1', 'This is the body of post 1', '2022-03-31 16:35:38', NULL, 1),
(2, 'Post 2', 'This is the body of post 2', '2022-03-31 16:35:38', NULL, 2),
(3, 'Post 3', 'This is the body of post 3', '2022-03-31 16:35:38', NULL, 3),
(4, 'Post 4', 'This is the body of post 4', '2022-03-31 16:35:38', NULL, 4),
(5, 'Post 5', 'This is the body of post 5', '2022-03-31 16:35:38', NULL, 5);
