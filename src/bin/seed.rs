extern crate gbbs;

use diesel::sql_query;
use diesel::prelude::*;
use std::fs;

fn main() {
  let conn = self::gbbs::establish_connection();
  let sql_dump = fs::read_to_string("seeds.sql").expect("Error reading the file");

  for statement in sql_dump.split(";") {
    if statement.trim() == "" {
      continue;
    }

    match sql_query(statement).execute(&conn) {
      Ok(num) => println!("Processed {} rows", num),
      Err(e) => println!("Error: {}", e)
    };
  }
}