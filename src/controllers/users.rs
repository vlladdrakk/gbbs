use log::*;
use crate::users;
use crate::Request;
use urlencoding::decode;

pub fn handle_create(request: &mut Request) {
  info!("handle_create");
  let user_id_opt = match &request.cert {
    Some(c) => users::create_with_cert(c.as_ref()),
    None => {
      panic!("Failed to get cert from request");
    }
  };

  match user_id_opt {
    Some(id) => {
      request.write_to_stream(&format!("30 /accounts/create/{} \r\n", id));
    },
    None => {
      request.write_to_stream("40 User account already exists \r\n");
    }
  };
}

pub fn handle_update_username(request: &mut Request, user_id: i32) {
  info!("handle_update_username, params: {}, {}", request.input, user_id);
  let decoded_name: String = decode(&request.input).expect("UTF-8").into_owned();
  users::update_username(user_id, decoded_name);

  request.write_to_stream("30 /accounts/complete \r\n");
}

pub fn handle_delete(request: &mut Request) {
  match &request.user {
    Some(u) => {
      info!("handle_delete, user_id: {}", u.id);
      match users::delete(u.id) {
        true => request.write_to_stream("30 / \r\n"),
        false => request.write_to_stream("40 Failed to delete the user \r\n")
      };
    }
    None => request.write_to_stream("40 User not logged in \r\n")
  }
}