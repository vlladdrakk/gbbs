use diesel::prelude::*;
use dotenv::dotenv;
use std::env;
use openssl::x509::X509Ref;
use openssl::hash::MessageDigest;
use base64;
use std::collections::HashMap;

pub fn establish_connection() -> SqliteConnection {
  dotenv().ok();

  let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
  SqliteConnection::establish(&database_url)
    .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn parse_cert(cert: &X509Ref) -> HashMap<&str, String> {
  let subject_names: Vec<String> = cert.subject_name().entries().map(|x| {
    match x.data().as_utf8() {
      Ok(n) => n.to_string(),
      Err(_) => panic!("Failed to get subject names from cert")
    }
  }).collect();

  let common_name = match subject_names.len() > 0 {
    true => subject_names[0].clone(),
    false => panic!("Failed to get common name from cert")
  };

  let digest = match cert.digest(MessageDigest::sha256()) {
    Ok(bytes) => base64::encode(bytes.as_ref()),
    Err(_) => panic!("Failed to get digest from cert")
  };

  let not_before = cert.not_before().to_string();
  let not_after = cert.not_after().to_string();
  let serial_number = match cert.serial_number().to_bn() {
    Ok(n) => n.to_string(),
    Err(_) => panic!("Failed to get serial number from cert")
  };

  return HashMap::from([
    ("common_name", common_name),
    ("digest", digest),
    ("not_before", not_before),
    ("not_after", not_after),
    ("serial", serial_number)
  ]);
}

pub fn get_fingerprint(cert: &X509Ref) -> String {
  return parse_cert(cert).get("digest").unwrap().to_string();
}
