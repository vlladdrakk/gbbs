table! {
    posts (id) {
        id -> Integer,
        title -> Text,
        body -> Text,
        created_at -> Timestamp,
        reply_to_id -> Nullable<Integer>,
        user_id -> Integer,
    }
}

table! {
    users (id) {
        id -> Integer,
        username -> Nullable<Text>,
        cert_fingerprint -> Text,
        not_valid_before -> Timestamp,
        not_valid_after -> Timestamp,
        common_name -> Text,
        created_at -> Timestamp,
    }
}

joinable!(posts -> users (user_id));

allow_tables_to_appear_in_same_query!(
    posts,
    users,
);
