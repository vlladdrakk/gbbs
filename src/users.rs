use diesel::prelude::*;
use crate::models::{User, UserForm};
use openssl::x509::X509Ref;
use chrono::NaiveDateTime;
use log::info;

pub fn create_with_cert(certificate: &X509Ref) -> Option<i32> {
  use crate::schema::users::dsl::*;
  use crate::schema::users;

  let cert_hash = crate::lib::parse_cert(certificate);
  let fmt_str = "%b %d %H:%M:%S %Y %Z";
  info!("timestamp: {}", cert_hash.get("not_before").unwrap());

  let user_exists = find_by_fingerprint(cert_hash.get("digest").unwrap()).len() > 0;

  if user_exists {
    return None;
  }

  let new_user = UserForm {
    cert_fingerprint: cert_hash.get("digest").unwrap(),
    not_valid_after: &NaiveDateTime::parse_from_str(&cert_hash.get("not_after").unwrap(), fmt_str).unwrap(),
    not_valid_before: &NaiveDateTime::parse_from_str(&cert_hash.get("not_before").unwrap(), fmt_str).unwrap(),
    common_name: cert_hash.get("common_name").unwrap()
  };
  let conn = crate::lib::establish_connection();
  let user: Result<Vec<User>, diesel::result::Error> = conn.transaction(|| {
    diesel::insert_into(users::table)
      .values(&new_user)
      .execute(&conn)
      .expect("Failed to create new user");

    let last_user = users.filter(id.gt(0))
      .order(id.desc())
      .limit(1)
      .load::<User>(&conn)
      .expect("Failed to get last user");

    Ok(last_user)
  });

  return Some(user.unwrap()[0].id);
}

pub fn update_username(user_id: i32, name: String) {
  use crate::schema::users::dsl::*;
  let conn = crate::lib::establish_connection();

  diesel::update(users.filter(id.eq(user_id)))
    .set(username.eq(name))
    .execute(&conn)
    .unwrap();
}

pub fn find_by_fingerprint(fingerprint: &str) -> Vec<User> {
  use crate::schema::users::dsl::*;
  let conn = crate::lib::establish_connection();
  let user_list = users.filter(cert_fingerprint.eq(fingerprint)).load::<User>(&conn).expect("Failed to filter by fp");

  return user_list;
}

pub fn find(user_id: i32) -> Option<User> {
  use crate::schema::users::dsl::*;
  let conn = crate::lib::establish_connection();
  let user_list = users.filter(id.eq(user_id))
    .load::<User>(&conn)
    .expect("Failed to filter by fp");

  if user_list.len() > 0 {
    Some(user_list[0].clone())
  } else {
    None
  }
}

pub fn delete(user_id: i32) -> bool {
  use crate::schema::users::dsl::*;
  let conn = crate::lib::establish_connection();
  let delete_count = diesel::delete(users.filter(id.eq(user_id)))
    .execute(&conn)
    .expect("Error deleting user");

  return delete_count > 0;
}
