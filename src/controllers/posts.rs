use diesel::prelude::*;
use crate::models::Post;
use crate::posts::{create, set_reply_to_id};
use log::*;
use urlencoding::decode;
use crate::{Request, search_engine};

pub fn handle_create(request: &mut Request) {
  info!("handle_create, params: N/A");
  request.write_to_stream("10 What is the title? \r\n");
}

pub fn handle_create_input(request: &mut Request) {
  info!("handle_create_input, params: input: {}", request.input);
  let conn = crate::lib::establish_connection();

  let post_id = create(&conn, &request.input, "", request.user.as_ref().unwrap().id);
  debug!("post_id in create handler: {}", post_id);

  if post_id == -1 {
    error!("Failed to create post, redirecting to error page");
    request.write_to_stream("40 DB failure \r\n");

    return;
  }

  // crate::write_to_stream(stream, "20 text/gemini \r\n");
  request.write_to_stream(&format!("30 /posts/create/body/{} \r\n", post_id));
}

pub fn handle_create_body(request: &mut Request, post_id: i32) {
  use crate::schema::posts::dsl::*;
  info!("handle_create_body, params: {}", request.input);

  if request.input == "" {
    info!("Requesting input from the user");
    request.write_to_stream("10 Body \r\n");

    return;
  }

  let conn = crate::lib::establish_connection();
  let decoded_body: String = decode(&request.input).expect("UTF-8").into_owned();

  let post: &Post = &posts.filter(id.eq(post_id))
    .limit(1)
    .load::<Post>(&conn)
    .expect("Failed to get post")[0];

  // Only add the body if one doesn't already exist. Prevent overwriting
  if post.body == "" {
    diesel::update(posts.filter(id.eq(post_id)))
      .set(body.eq(decoded_body))
      .execute(&conn)
      .unwrap();
  }

  info!("Updated post {}", post_id);

  // Send the new post id to the indexer
  request.idx_ch.send(post_id).unwrap();

  let redirect_post_id = match post.reply_to_id {
    Some(reply_id) => reply_id,
    None => post.id
  };

  request.write_to_stream(&format!("30 /posts/{} \r\n", redirect_post_id));
}

pub fn handle_get(request: &mut Request, post_id: i32){
  use crate::schema::posts::dsl::*;
  info!("handle_get, id: {}", post_id);

  let conn = crate::lib::establish_connection();
  let post: &Post = &posts.filter(id.eq(post_id))
    .limit(1)
    .load::<Post>(&conn)
    .expect("Failed to get post")[0];

  let replies: Vec<Post> = posts.filter(reply_to_id.eq(post_id)).load::<Post>(&conn).expect("Failed to get replies");
  request.render_data = post.to_hash();

  request.render_data.insert(String::from("replies"), crate::posts::render_replies(replies));
  request.view = String::from("post");
}

pub fn handle_create_reply(request: &mut Request, op_id: i32) {
  use crate::schema::posts::dsl::*;
  info!("handle_create_reply: op_id: {}", op_id);

  let conn = crate::lib::establish_connection();
  let post: &Post = &posts.filter(id.eq(op_id))
    .limit(1)
    .load::<Post>(&conn)
    .expect("Failed to find op post")[0];

  let new_title = format!("RE: {}", post.title);
  let new_post_id = create(&conn, &new_title, "", request.user.as_ref().unwrap().id);

  set_reply_to_id(new_post_id, post.id);

  request.write_to_stream(&format!("30 /posts/create/body/{} \r\n", new_post_id));
}

pub fn handle_search(request: &mut Request) {
  use crate::schema::posts::dsl::*;
  let query = decode(&request.input).expect("UTF-8").into_owned();
  info!("handle_search: query: {}", query);
  request.view = String::from("search");

  let result_ids = search_engine::search_posts(&query);
  let conn = crate::lib::establish_connection();
  let post_list: Vec<Post> = posts.filter(id.eq_any(result_ids))
    .load::<Post>(&conn)
    .expect("Failed to load posts from search");

  let mut post_str = String::new();
  for p in post_list {
    post_str.push_str(&crate::posts::to_gmni_string(&p));
    post_str.push_str("\n");
  }

  request.render_data.insert(String::from("results"), post_str.trim().to_string());
  request.render_data.insert(String::from("query"), query.clone());
}