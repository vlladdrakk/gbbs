use diesel::prelude::*;
use crate::models::{PostForm, Post};
use urlencoding::decode;

pub fn create(conn: &SqliteConnection, title_in: &str, body_in: &str, uid: i32) -> i32 {
  use crate::schema::posts;
  use crate::schema::posts::dsl::*;

  let decoded_title: String = decode(title_in).expect("UTF-8").into_owned();
  let decoded_body: String = decode(body_in).expect("UTF-8").into_owned();

  let new_post = PostForm {
    title: &decoded_title,
    body: &decoded_body,
    user_id: &uid
  };

  let post: Result<Vec<Post>, diesel::result::Error> = conn.transaction(|| {
    diesel::insert_into(posts::table)
      .values(&new_post)
      .execute(conn)
      .expect("Failed to create new post");

    let last_post = posts.filter(id.gt(0))
      .order(id.desc())
      .limit(1)
      .load::<Post>(conn)
      .expect("Failed to get last id");

    Ok(last_post)
  });

  return post.unwrap()[0].id;
}

pub fn set_reply_to_id(post_id: i32, op_id: i32) {
  use crate::schema::posts::dsl::*;
  let conn = crate::lib::establish_connection();

  diesel::update(posts.filter(id.eq(post_id)))
    .set(reply_to_id.eq(op_id))
    .execute(&conn)
    .unwrap();
}

pub fn get_latest(limit: i32) -> Vec<String> {
  use crate::schema::posts::dsl::*;

  let conn = crate::lib::establish_connection();

  let latest_posts = posts.limit(limit.into())
    .order(id.desc())
    .load::<Post>(&conn)
    .expect("Failed to load latest posts");

  return latest_posts.iter().map(to_gmni_string).collect();
}

pub fn get_paginated(page: i32, limit: i32) -> String {
  use crate::schema::posts::dsl::*;

  let conn = crate::lib::establish_connection();
  let post_list = posts.limit(limit.into())
    .order(id.desc())
    .offset(((page - 1) * limit).into())
    .load::<Post>(&conn)
    .expect("Failed to load paginated posts");

  let string_list: Vec<String> = post_list.iter().map(to_gmni_string).collect();
  return string_list.join("\n");
}

pub fn to_gmni_string(post: &Post) -> String {
  crate::views::render_to_string("compact_post", &post.to_hash())
}

pub fn render_replies(replies: Vec<Post>) -> String {
  let mut result: String = String::new();

  for reply in replies {
    let reply_string = crate::views::render_to_string("post_reply", &reply.to_hash());

    result.push_str(&reply_string);
    result.push_str("\n");
  }

  return result.trim_end().to_string();
}

pub fn count() -> i32 {
  use crate::schema::posts::dsl::*;
  let conn = crate::lib::establish_connection();

  match posts.count().get_result::<i64>(&conn) {
    Ok(count) => count as i32,
    Err(_) => panic!("Failed to count posts in db")
  }
}
