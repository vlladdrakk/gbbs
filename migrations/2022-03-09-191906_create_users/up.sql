-- Your SQL goes here
CREATE TABLE IF NOT EXISTS users (
  id integer not null primary key,
  username varchar,
  cert_fingerprint text not null,
  not_valid_before datetime not null,
  not_valid_after datetime not null,
  common_name varchar not null,
  created_at datetime not null DEFAULT CURRENT_TIMESTAMP
)