-- Your SQL goes here
CREATE TABLE IF NOT EXISTS posts (
  id integer not null primary key,
  title varchar not null,
  body text not null,
  created_at datetime not null DEFAULT CURRENT_TIMESTAMP
)