use crate::*;
use crate::controllers;
use log::info;
use regex::Regex;

pub type Response = (String, HashMap<String, String>);

pub fn view_only_response(view: &str) -> Response {
  return Response::from((String::from(view), HashMap::new()));
}

static PAGE_SIZE: i32 = 10;

pub fn handle(request: &mut Request) {//stream: &mut SslStream<TcpStream>, route_in: &str, is_input: bool, input: &str) -> Response {
  let mut route = request.path.clone();
  let is_input = request.is_input;
  route.insert_str(0, "/");
  info!("route: {}", &route);

  if &route == "/" {
    let posts: Vec<String> = posts::get_latest(PAGE_SIZE);

    for (i, p) in posts.iter().enumerate() {
      request.render_data.insert(format!("post-{}", i).to_string(), p.to_string());
    }

    request.view = String::from("home");
  }
  else if &route == "/about" {
    request.view = String::from("about");
  }
  else if &route == "/login" {
    match request.cert {
      Some(_) => request.write_to_stream("30 / \r\n"),
      None => request.write_to_stream("60 Provide cert for login \r\n")
    }
  }
  else if &route == "/unauthorized" {
    request.view = String::from("unauthorized");
  }
  else if &route == "/accounts" {
    request.view = String::from("accounts");
  }
  else if &route == "/accounts/profile" {
    if !request.logged_in {
      request.write_to_stream("51 Not logged in \r\n")
    } else {
      request.view = String::from("account_profile");
    }
  }
  else if &route == "/accounts/complete" {
    request.view = String::from("accounts_complete");
  }
  else if &route == "/accounts/create" {
    match request.cert {
      Some(_) => {
        controllers::users::handle_create(request);
      },
      None => {
        request.write_to_stream("60 Cert needed for account creation \r\n");
      }
    };
  }
  else if &route == "/accounts/update/username" {
    match request.is_input {
      true => {
        let user = match &request.user {
          Some(u) => u.clone(),
          None => panic!("User attempting to update username is not logged in")
        };
        controllers::users::handle_update_username(request, user.id);
      },
      false => request.write_to_stream("10 New username: \r\n")
    };
  }
  else if &route == "/accounts/delete" {
    if request.logged_in {
      controllers::users::handle_delete(request)
    } else {
      request.write_to_stream("20 ")
    }
  }
  else if &route == "/search" {
    if request.is_input {
      controllers::posts::handle_search(request);
    } else {
      request.write_to_stream("10 Search query \r\n");
    }
  }
  else if Regex::new(r"/accounts/create/\d.*").unwrap().is_match(&route) {
    let cap = Regex::new(r"/accounts/create/(?P<id>\d+).*").unwrap().captures(&route).unwrap();
    info!("id: {}", &cap[1]);
    let id = cap[1].to_string().parse::<i32>().unwrap();

    if is_input {
      controllers::users::handle_update_username(request, id);
    } else {
      request.write_to_stream("10 Username \r\n");
    }
  }
  else if &route == "/all" {
    let mut page: i32 = 1;
    let post_count: i32 = posts::count();

    if is_input {
      page = request.input.parse::<i32>().unwrap();
    }

    if post_count < page * PAGE_SIZE {
      page = 1;
    }

    request.render_data.insert(String::from("post_list"), posts::get_paginated(page, PAGE_SIZE));
    request.render_data.insert(String::from("page_number"), page.to_string());

    if page > 1 {
      request.render_data.insert(String::from("prev_page_number"), (page - 1).to_string());
    }

    if post_count - (page * PAGE_SIZE) > 0 {
      request.render_data.insert(String::from("next_page_number"), (page + 1).to_string());
    }

    request.view = String::from("paginated_posts");
  }
  else if Regex::new(r"/posts/create$").unwrap().is_match(&route) {
    if !request.logged_in {
      request.write_to_stream("30 /unauthorized \r\n");
      return;
    }

    if is_input {
      return controllers::posts::handle_create_input(request);
    } else {
      return controllers::posts::handle_create(request);
    }
  }
  else if Regex::new(r"/posts/create/body/\d.*").unwrap().is_match(&route) {
    if !request.logged_in {
      request.write_to_stream("30 /unauthorized \r\n");
      return;
    }

    let cap = Regex::new(r"/posts/create/body/(?P<id>\d+).*").unwrap().captures(&route).unwrap();
    info!("id: {}", &cap[1]);
    let id = cap[1].to_string().parse::<i32>().unwrap();

    return controllers::posts::handle_create_body(request, id);
  }
  else if Regex::new(r"/posts/\d+$").unwrap().is_match(&route) {
    let cap = Regex::new(r"/posts/(?P<id>\d+)$").unwrap().captures(&route).unwrap();
    let id = cap[1].to_string().parse::<i32>().unwrap();

    return controllers::posts::handle_get(request, id);
  }

  else if Regex::new(r"/posts/\d+/reply$").unwrap().is_match(&route) {
    if !request.logged_in {
      request.write_to_stream("30 /unauthorized \r\n");
      return;
    }

    let cap = Regex::new(r"/posts/(?P<id>\d+)/reply$").unwrap().captures(&route).unwrap();
    let id = cap[1].to_string().parse::<i32>().unwrap();

    return controllers::posts::handle_create_reply(request, id);
  }

}