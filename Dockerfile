FROM rust:1.59-slim

RUN apt update && apt install -y pkg-config libssl-dev libsqlite3-dev

WORKDIR /usr/src/gbbs

ENV DATABASE_URL=/opt/database
ENV PRIV_KEY=/opt/certs/ca.key
ENV CERT_CHAIN=/opt/certs/ca.cer

COPY . .
RUN rm -r target
RUN cargo build --release

RUN mkdir -p /opt/certs
RUN cargo install diesel_cli --no-default-features --features sqlite
RUN diesel database setup
RUN diesel migration run

EXPOSE 1965

CMD [ "./target/release/gbbs" ]
