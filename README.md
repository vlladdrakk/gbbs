# GBBS
## What is it?
GBBS (or gbbs) is a bulletin board system that runs on the Gemini protocol. I built this so that I could learn Rust, so the code is pretty bad. I've also never used a BBS irl and couldn't be bothered to spend more than a couple minutes of googling to understand how they worked. I based this program off of how BBSs work in the Megaman Battle Network games, plus some modern features sprinkled on top (How can you follow anything if replies aren't shown as a thread?).

The basic architecture of the program is MVC (model view controller) since have a background in ruby on rails. Each request is handled by it's own thread and each gemtext response is rendered with handlebars. I also added a 'search engine' using sqlite3's FTS5 virtual table. To index posts, when the program is started an indexer thread fires up and makes sure all posts are loaded into the virtual table, then continues to run, waiting for ids of new posts to be sent to it through a mpsc channel. That was pretty fun to implement.

## Features
 - [x] Submitting new posts
 - [x] Replying to posts
 - [x] Threaded view of replies
 - [x] Authentication with certificates
 - [x] Basic search
 - [ ] Changing the cert for on your account
 - [ ] Changing your username
 - [ ] Public user page with all their posts and replies (filterable)
 - [ ] Notifications (for replies)
 - [ ] Page load counter (for authors)

## Contribution
If you want to dive into the code, figure things out, and make a change. Go for it! I'd be happy to get contributions on this project, especially if it involves cleaning up or improving the code. If you want to rewrite the whole thing (or just portions), giv'er! I might be a bit picky on new features that aren't in the spirit of the project (which is undefined and subject to the whims of...well me) but don't let that stop you.

Issues are also a great help! I've only tested the application on Elpher (emacs gemini client) and Lagrange, so there are probably lots of bugs with other clients. I can't guarantee that I will be able to get to the issues with any speed, I do have a full time job and family, but I'll probably read them all!

## Development Setup
For the most basic setup all you need is to have rust installed, generate a server certificate, and configure the environment variables.

1. Instructions for installing rust: https://www.rust-lang.org/tools/install
2. Generate a server certificate with the helper script:
```
./scripts/create_server_certs.sh
```
3. The environment variables are stored in `.env` and can be changed to point to wherever your db or certs are located. You can also set the variables per run (e.g. `DATABASE_URL=./database.db cargo run`).
4. Install the diesel cli `cargo install diesel_cli --no-default-features --features sqlite`
5. Setup the database `diesel database setup`
6. Seed the database with test data by running `cargo run --bin seed`
7. Now you can run the software with `cargo run`

## Docker Setup

Steps:
1. Build the image: `docker build . -t gbbs`
2. Generate a server certificate with the helper script:
  ```
  ./scripts/create_server_certs.sh
  ```
  *OR*
  Use some certs that you already have.

3. Create the docker container: `docker run -d -v $(pwd)/certs:/opt/certs -p 1965:1965 gbbs` If your certs are stored elsewhere, change the volume mount.