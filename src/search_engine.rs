use log::info;
use diesel::sql_query;
use diesel::prelude::*;
use diesel::sql_types::{Integer, Text};
use std::sync::mpsc::Receiver;

#[derive(QueryableByName)]
struct SearchResultId {
    #[sql_type = "Integer"]
    id: i32
}

pub fn search_posts(query: &str) -> Vec<i32> {
  let conn = crate::lib::establish_connection();
  let results: Vec<SearchResultId> = match sql_query("select post_id as id from post_search where content match ? order by rank")
    .bind::<Text, _>(query)
    .get_results(&conn) {
    Ok(r) => r,
    Err(e) => panic!("Failed to execute search for {}, got error: {}", query, e)
  };

  return results.into_iter().map(|x| x.id).collect();
}

pub fn load_fts_table() {
  let conn = crate::lib::establish_connection();

  // Create the fuzzy search table
  sql_query(
    "create virtual table if not exists post_search using fts5(post_id, content, tokenize = 'porter ascii');"
  ).execute(&conn).unwrap();


  // Populate the fuzzy search table
  sql_query(
    "insert into post_search select id as post_id, title || \" \" || body as content from posts where id not in (select post_id from post_search);"
  ).execute(&conn).unwrap();
}

fn find_and_remove(conn: &SqliteConnection, post_id: i32) {
  let results: Vec<SearchResultId> = match sql_query("select post_id as id from post_search where post_id=?")
    .bind::<Integer, _>(post_id)
    .get_results(conn) {
      Ok(r) => r,
      Err(_) => Vec::new()
    };

  if results.len() > 0 {
    sql_query("delete from post_search where post_id=?").bind::<Integer, _>(post_id).execute(conn).unwrap();
  }
}

pub fn indexer_run(rx: Receiver<i32>) {
  info!("Indexer thread started!");
  loop {
    let post_id = rx.recv().unwrap();
    info!("Indexer received: {}", post_id);
    let conn = crate::lib::establish_connection();
    // Assume if the post is already indexed that we need to reindex it
    find_and_remove(&conn, post_id);

    sql_query(
      "insert into post_search select id as post_id, title || \" \" || body as content from posts where id=?"
    ).bind::<Integer, _>(post_id).execute(&conn).unwrap();
  }
}
