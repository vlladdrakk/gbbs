# Unauthorized access attempt
You are not authorized to access this page. You may need to log in.

=> gemini://{{ host }}/login login
