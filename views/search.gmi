# Search

{{ #if query }}
Query: {{ query }}
=> gemini://{{ host }}/search Search again
{{ /if }}

{{ #if results }}
## Results

{{ results }}
{{ /if }}