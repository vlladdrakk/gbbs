#[macro_use]
extern crate diesel;
extern crate log;

pub mod models;
pub mod schema;
pub mod routes;
pub mod controllers;
pub mod search_engine;
mod views;
mod lib;
mod posts;
mod users;

use std::net::{TcpListener, TcpStream};
use openssl::ssl::{
  SslMethod, SslAcceptor, SslStream, SslFiletype, SslVerifyMode, SslSessionCacheMode, SslOptions
};
use std::sync::Arc;
use std::sync::mpsc::{channel, Sender};
use std::thread;
use std::io::{Read, Write};
use std::str;
use std::collections::HashMap;
use views::RenderError;
use env_logger::Env;
use log::*;
use models::Post;
use openssl::x509::X509;
use models::User;
use dotenv::dotenv;
use std::env;

impl Post {
  fn to_hash(&self) -> HashMap<String, String> {
    let user = users::find(self.user_id);

    HashMap::from([
      (String::from("id"), self.id.to_string()),
      (String::from("title"), self.title.clone()),
      (String::from("body"), self.body.clone()),
      (String::from("created_at"), self.created_at.format("%Y-%m-%d %H:%M:%S").to_string()),
      (String::from("reply_to_id"), match self.reply_to_id {
        Some(x) => x.to_string(),
        None => String::new()
      }),
      (String::from("author_name"), match user {
        Some(u) => u.username.unwrap_or(String::from("Anon (error)")),
        None => String::from("Anon (error)")
      })
    ])
  }
}

pub struct Request<'a> {
  stream: &'a mut SslStream<TcpStream>,
  cert: Option<X509>,
  path: String,
  input: String,
  is_input: bool,
  render_data: HashMap<String, String>,
  view: String,
  user: Option<User>,
  logged_in: bool,
  idx_ch: Sender<i32> // Sender channel for the indexer. I think this will be the easiest way to pass around senders.
}

impl Request<'_> {
  fn set_cert(&mut self) {
    self.cert = self.stream.ssl().peer_certificate();
  }

  fn write_to_stream(&mut self, data: &str) {
    match self.stream.write(data.as_bytes()) {
      Err(e) => {
        panic!("Error writing to stream, {:?}", e);
      },
      _ => ()
    }
  }
}

fn response_header(code: &str) -> String {
  let message = format!("{} text/gemini \r\n", code);

  return message;
}

pub fn write_to_stream(stream: &mut SslStream<TcpStream>, data: &str) {
  match stream.write(data.as_bytes()) {
    Err(e) => {
      panic!("Error writing to stream, {:?}", e);
    },
    _ => ()
  }
}

fn handle_client(mut stream: SslStream<TcpStream>, indexer_channel: Sender<i32>) {
  // Read the initial request into the buffer
  let mut buffer = [0;1024];
  stream.read(&mut buffer[..]).unwrap();

  // Get the url from the request
  let url = str::from_utf8(&buffer).unwrap();
  let host = url.replace("gemini://", "");
  let path: Vec<&str> = host.splitn(2, "/").collect();
  let mut route: &str = "/";
  let mut is_input: bool = false;
  let input: &str;

  info!(target: "main", "Request: {}", url);

  if path.len() == 2 {
    route = path[1].trim_end_matches('\0').trim();

    route.to_string().insert_str(0, "/");
  }

  if route.contains('?') {
    let route_parts: Vec<&str> = route.splitn(2, "?").collect();

    route = route_parts[0];
    is_input = true;
    input = route_parts[1];
  } else {
    input = "";
  }

  let mut request = Request {
    stream: &mut stream,
    cert: None,
    path: route.to_string(),
    input: input.to_string(),
    is_input,
    render_data: HashMap::new(),
    view: String::new(),
    user: None,
    logged_in: false,
    idx_ch: indexer_channel
  };

  request.set_cert();

  match request.cert {
    Some(ref c) => {
      let fingerprint = crate::lib::get_fingerprint(c);
      let user_list = users::find_by_fingerprint(&fingerprint);
      info!(target: "main", "Loaded cert {}", fingerprint);

      if user_list.len() == 0 {
        info!(target: "main", "user not found!");
        request.user = None;
        request.logged_in = false;
      } else {
        info!(target: "main", "user found!");
        request.user = Some(user_list[0].clone());
        request.logged_in = true;
      }
    },
    None => request.user = None
  };

  // call route handler
  routes::handle(&mut request);

  if request.view == "" {
    info!(target: "main", "No view to return, done handling!");
    return;
  }

  let rendered_view = match views::render(&request) {
    Ok(result) => {
      write_to_stream(&mut stream, &response_header("20"));
      result
    },
    Err(e) => match e {
      RenderError::ViewNotFound => {
        write_to_stream(&mut stream, &response_header("51"));
        "".to_string()
      }
    }
  };

  write_to_stream(&mut stream, &rendered_view);
  stream.shutdown().unwrap();
}

fn init_logger() {
  let env = Env::default().filter_or("LOG_LEVEL", "info");

  env_logger::init_from_env(env);
}

fn init_search_engine() -> Sender<i32> {
  search_engine::load_fts_table();
  info!(target: "main", "Search engine initialized!");

  let (tx, rx) = channel::<i32>();
  info!(target: "main", "Starting indexer thread");
  thread::spawn(move || {
    search_engine::indexer_run(rx);
  });

  return tx;
}

fn main() -> std::io::Result<()> {
  dotenv().ok();
  let listener = TcpListener::bind("0.0.0.0:1965")?;
  let chain_file: String;
  let privkey_file: String;

  init_logger();
  let sender = init_search_engine();

  match env::var("PRIV_KEY") {
    Ok(val) => {
      privkey_file = val;
    },
    Err(_) => panic!("Unable to load keys, priv_key is not set")
  };

  match env::var("CERT_CHAIN") {
    Ok(val) => {
      chain_file = val;
    },
    Err(_) => panic!("Unable to load keys, cert_chain is not set")
  };

  let mut acceptor = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
  acceptor.set_private_key_file(&privkey_file, SslFiletype::PEM).unwrap();
  acceptor.set_certificate_chain_file(&chain_file).unwrap();
  acceptor.check_private_key().unwrap();
  acceptor.set_verify_callback(SslVerifyMode::PEER, |_, _| true);
  acceptor.set_session_cache_mode(SslSessionCacheMode::OFF);
  acceptor.set_options(SslOptions::NO_TICKET);
  let arc_acceptor = Arc::new(acceptor.build());


  // accept connections and process them serially
  for stream in listener.incoming() {
    match stream {
      Ok(stream) => {
        let acceptor = arc_acceptor.clone();
        let index_sender = sender.clone();

        thread::spawn(move || {
          let stream = acceptor.accept(stream).unwrap();
          handle_client(stream, index_sender);
        });
      }
      Err(e) => {
        error!("{}", e);
      }
    };
  }

  Ok(())
}
