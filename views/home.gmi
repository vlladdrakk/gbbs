# Welcome to GBBS
## A BBS for Gemini

=> gemini://{{ host }}/search Search posts
{{ #if logged_in }}
=> gemini://{{ host }}/posts/create Submit a new post
{{ /if }}
{{ #unless logged_in }}
=> gemini://{{ host }}/accounts Create an account
=> gemini://{{ host }}/login Login
{{ /unless }}

## Latest posts

{{ #if post-0 }}
{{ post-0 }}
{{ /if }}
{{ #if post-1 }}
{{ post-1 }}
{{ /if }}
{{ #if post-2 }}
{{ post-2 }}
{{ /if }}
{{ #if post-3 }}
{{ post-3 }}
{{ /if }}
{{ #if post-4 }}
{{ post-4 }}
{{ /if }}
{{ #if post-5 }}
{{ post-5 }}
{{ /if }}
{{ #if post-6 }}
{{ post-6 }}
{{ /if }}
{{ #if post-7 }}
{{ post-7 }}
{{ /if }}
{{ #if post-8 }}
{{ post-8 }}
{{ /if }}
{{ #if post-9 }}
{{ post-9 }}
{{ /if }}
